import React from 'react'
const student = {
  name: 'John',
  standard: '10',
  section: 'B'
}
const teacher = {
  name: 'Jack',
  standard: '10',
  section: 'B'
}
 
const studentTeacherArray = [student, teacher]

export function MyComponent({integerValue}) {
  // renmaiming name key
  // const {name} = teacher

  const [student, teacher] = studentTeacherArray

  const arr1 = [1,2,3]
  const arr2 = [4,5,6]
  //merging array using spread operator
  const finalArr = [...arr1, ...arr2]
  console.log(finalArr)

  const {name: studentName, standard, section} = student

  console.log(teacher)
  console.log("spread operator", {...student, name: 'Joe', address: "teku"})

  const key = "name"
  //accessing object properties
  console.log(studentTeacherArray[0][key])
  console.log(studentTeacherArray[1].name)
  
  return <h1>Hello {studentName} {standard}({section})</h1>
}

export const MyAnotherComponent = props => <h1>Hello World {props.integerValue + 1}</h1>

export default MyComponent

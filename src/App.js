import axios from 'axios'
import { useEffect, useState } from 'react'
import './App.css'
import { Form } from './components/Form'

const App = () => {
  const [students, setStudents] = useState([])

  const addStudent = (student) => {
    student['userDetail'] = {
      education: student['education'],
      qualification: student['qualification'],
      experience: student['experience'],
    }
    student['enabled'] = true
    axios
      .post('http://localhost:8081/user', student)
      .then((res) => {
        setStudents([...students, res.data])
      })
      .catch((err) => {})
      .finally()
  }

  useEffect(() => {
    axios.get('http://localhost:8081/user').then((res) => {
      setStudents(res.data)
    })
  }, [])

  return (
    <>
      <Form addStudent={addStudent} />
      <div className="row">
        {students.map((student, idx) => (
          <div key={idx}>{student.fullname}</div>
        ))}
      </div>
    </>
  )
}

// ternary operator
// condition1 ? statement1 : statement3

export default App

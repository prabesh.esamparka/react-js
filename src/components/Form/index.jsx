import { useState } from 'react'

export const Form = (props) => {
  const [form, setForm] = useState({
    fullname: '',
    email: '',
    address: '',
    education: '',
    qualification: '',
    experience: '',
  })

  const handleSubmit = (e) => {
    e.preventDefault()
    props.addStudent(form)
    setForm({
      fullname: '',
      email: '',
      address: '',
      education: '',
      qualification: '',
      experience: '',
    })
  }

  const handleChange = (e) => {
    const changedValue = {}
    changedValue[e.target.name] = e.target.value
    setForm({ ...form, ...changedValue })
  }

  return (
    <form onSubmit={handleSubmit}>
      <input
        name="fullname"
        type={'text'}
        onChange={handleChange}
        value={form['fullname']}
        required
      />
      <input
        name="email"
        type={'text'}
        onChange={handleChange}
        value={form['email']}
        required
      />
      <input
        name="address"
        type={'text'}
        onChange={handleChange}
        value={form['address']}
        required
      />
      <input
        name="experience"
        type={'text'}
        onChange={handleChange}
        value={form['experience']}
        required
      />
      <input
        name="qualification"
        type={'text'}
        onChange={handleChange}
        value={form['qualification']}
        required
      />
      <input
        name="education"
        type={'text'}
        onChange={handleChange}
        value={form['education']}
        required
      />
      <button type="submit" value={2}>
        Submit
      </button>
    </form>
  )
}
